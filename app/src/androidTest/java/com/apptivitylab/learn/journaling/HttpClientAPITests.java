package com.apptivitylab.learn.journaling;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.apptivitylab.learn.journaling.api.BackendlessUploadRequest;
import com.apptivitylab.learn.journaling.api.RestAPIClient;
import com.apptivitylab.learn.journaling.model.Journal;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.*;

/**
 * Created by Admin on 07/10/2016.
 */

@RunWith(AndroidJUnit4.class)
public class HttpClientAPITests {

    private File getTestFile() {
        Context appContext = InstrumentationRegistry.getTargetContext();

        Bitmap bitmap = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.background);
        File targetFile = new File(appContext.getFilesDir(), "test.jpg");

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(targetFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
            fileOutputStream.close();

            return targetFile;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Test
    public void uploadRequestShouldUploadFile() throws Exception {
        final CountDownLatch signal = new CountDownLatch(1);

        Context appContext = InstrumentationRegistry.getTargetContext();
        RequestQueue queue = Volley.newRequestQueue(appContext);

        File targetFile = getTestFile();
        assertNotNull(targetFile);
        assertTrue(targetFile.exists());

        String path = "http://api.backendless.com/v1/files/media/" + targetFile.getName() + "?overwrite=true";

        BackendlessUploadRequest uploadRequest = new BackendlessUploadRequest(
                Request.Method.POST,
                path,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        signal.countDown();
                        assertNotNull(response);
                        assertTrue(response.has("fileURL"));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        signal.countDown();
                        assertNull(error);
                    }
                }
        );
        uploadRequest.setFile(targetFile, "upload", "image/jpeg");

        queue.add(uploadRequest);
        signal.await();
    }

    @Test
    public void clientUploadMethodShouldUploadFile() throws Exception {

        final CountDownLatch signal = new CountDownLatch(1);

        File targetFile = getTestFile();
        assertNotNull(targetFile);
        assertTrue(targetFile.exists());

        RestAPIClient.getInstance(InstrumentationRegistry.getTargetContext()).uploadFile(targetFile, "image/jpeg", new RestAPIClient.OnUploadFileCompleteListener() {
            @Override
            public void onUploadComplete(String path, VolleyError error) {
                signal.countDown();
                assertNotNull(path);
            }
        });

        signal.await();
    }

    @Test
    public void shouldLoadStringFromURL() throws Exception {
        final CountDownLatch signal = new CountDownLatch(1);

        String url = "https://developer.android.com/index.html";

        // Prepare a requestQueue
        Context appContext = InstrumentationRegistry.getTargetContext();
        RequestQueue requestQueue = Volley.newRequestQueue(appContext);

        StringRequest request = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                signal.countDown();
                assertNotNull(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                signal.countDown();
                assertNull(error);
            }
        });

        requestQueue.add(request);
        signal.await();
    }

}
