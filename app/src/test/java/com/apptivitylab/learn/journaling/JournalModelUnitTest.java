package com.apptivitylab.learn.journaling;

import com.apptivitylab.learn.journaling.model.Journal;

import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

/**
 * Created by Admin on 07/10/2016.
 */

public class JournalModelUnitTest {
    @Test
    public void defaultConstructorShouldCreateUntitled() {
        Journal journal = new Journal();
        assertEquals(journal.getTitle(), "Untitled");
    }

    @Test
    public void constructorShouldAcceptTitle() {
        Journal journal = new Journal("Some Title", "Body goes here", Calendar.getInstance());

        assertNotNull(journal);
        assertEquals(journal.getTitle(), "Some Title");
    }
}
