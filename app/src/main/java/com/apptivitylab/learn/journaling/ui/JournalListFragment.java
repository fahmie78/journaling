package com.apptivitylab.learn.journaling.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.apptivitylab.learn.journaling.R;
import com.apptivitylab.learn.journaling.api.RestAPIClient;
import com.apptivitylab.learn.journaling.model.Journal;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class JournalListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, JournalViewHolder.OnSelectJournalListener {
    private static final int ADD_ENTRY_REQUEST = 1;
    public static final String EXTRA_JOURNAL_ID = "journal_id";

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private JournalListAdapter mJournalListAdapter;
    private TextView mTextView;

    public JournalListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_journal_list, container, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.fragment_journal_swipe_refresh);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.fragment_journal_list_rv_journals);
        mTextView = (TextView) rootView.findViewById(R.id.fragment_journal_tv_no_journals);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Swipe Refresh Layout gets new data
        mSwipeRefreshLayout.setOnRefreshListener(this);

        // Prepare layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);

        // Prepare adapter
        if (mJournalListAdapter == null) {
            mJournalListAdapter = new JournalListAdapter(this);
        } else {
            mTextView.setVisibility(View.INVISIBLE);
        }

//        mJournalListAdapter.setJournalList(JournalLoader.loadSamples(getContext()));

        // Uncomment the following if you want to load data from JSON textfile
//        List<Journal> journalList = new ArrayList<>();
//        journalList.add(JournalLoader.loadJSONSample(getContext()));
//        mJournalListAdapter.setJournalList(journalList);

        mRecyclerView.setAdapter(mJournalListAdapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_add_entry, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            Intent intent = new Intent(getContext(), AddEntryActivity.class);
            startActivityForResult(intent, ADD_ENTRY_REQUEST);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_ENTRY_REQUEST && resultCode == Activity.RESULT_OK) {
            String journalId = data.getStringExtra(JournalListFragment.EXTRA_JOURNAL_ID);
            Snackbar.make(getView(), "Journal added! ID: " + journalId, Snackbar.LENGTH_SHORT).show();

            mTextView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onRefresh() {
        RestAPIClient.getInstance(getContext()).getJournals(new RestAPIClient.OnGetJournalsCompletedListener() {
            @Override
            public void onComplete(List<Journal> journals, VolleyError error) {
                if (error != null) {
                    Snackbar.make(getView(), error.getLocalizedMessage(), Snackbar.LENGTH_LONG).show();
                } else {
                    mJournalListAdapter.setJournalList(journals);
                    mJournalListAdapter.notifyDataSetChanged();
                    mTextView.setVisibility(View.INVISIBLE);
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void onJournalSelected(Journal journal) {
        JournalDetailFragment fragment = JournalDetailFragment.newInstance(journal);

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_main_container, fragment)
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();

    }
}
