package com.apptivitylab.learn.journaling.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.apptivitylab.learn.journaling.R;
import com.apptivitylab.learn.journaling.model.Journal;

import java.text.SimpleDateFormat;

/**
 * Created by Admin on 26/09/2016.
 */

public class JournalViewHolder extends RecyclerView.ViewHolder {
    public interface OnSelectJournalListener {
        void onJournalSelected(Journal journal);
    }

    private TextView mTitleTextView;
    private TextView mBodyTextView;
    private Journal mJournal;

    public JournalViewHolder(View itemView, final OnSelectJournalListener listener) {
        super(itemView);

        mTitleTextView = (TextView) itemView.findViewById(R.id.cell_journal_title);
        mBodyTextView = (TextView) itemView.findViewById(R.id.cell_journal_body);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onJournalSelected(mJournal);
            }
        });

    }

    public void setJournalEntry(@NonNull Journal journal) {
        mJournal = journal;
        mTitleTextView.setText(journal.getTitle());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String creationDate = simpleDateFormat.format(journal.getCreationDate().getTime());

        mBodyTextView.setText(creationDate + " - " + journal.getBody());
    }
}