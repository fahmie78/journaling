package com.apptivitylab.learn.journaling.controller;

import android.content.Context;
import android.util.Log;

import com.apptivitylab.learn.journaling.R;
import com.apptivitylab.learn.journaling.model.Journal;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Admin on 26/09/2016.
 */

public class JournalLoader {
    private static final String TAG = "JournalLoader";

    public static List<Journal> loadSamples(Context context) {
        ArrayList<Journal> sampleJournals = new ArrayList<>();

        InputStream inputStream = context.getResources().openRawResource(R.raw.journals);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split(";");

                String dateString = tokens[0];
                String title = tokens[1];
                String body = tokens[2];

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Calendar creationDate = Calendar.getInstance();
                creationDate.setTime(dateFormat.parse(dateString));

                Journal journal = new Journal(title, body, creationDate);
                sampleJournals.add(journal);
            }

        } catch (Exception exception) {

            Log.e(TAG, "Load exception: " + exception.toString());
        }

        return sampleJournals;
    }

    public static Journal loadJSONSample(Context context) {
        InputStream inputStream = context.getResources().openRawResource(R.raw.journal);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String line;
        JSONObject jsonObject = new JSONObject();
        Journal journal = new Journal();

        try {
            while ((line = reader.readLine()) != null) {
                jsonObject = new JSONObject(line);

                journal = new Journal(jsonObject);
            }
        } catch (Exception e) {
            Log.e(TAG, "loadJson exception: " + e.toString());
        }

        return journal;
    }
}
