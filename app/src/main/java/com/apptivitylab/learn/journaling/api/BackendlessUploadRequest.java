package com.apptivitylab.learn.journaling.api;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * BackendlessUploadRequest
 * Journaling-Android
 * <p>
 * Created by Li Theen Kok on 15/10/2016/10/2016.
 * Copyright (c) 2016 Apptivity Lab. All Rights Reserved.
 */

public class BackendlessUploadRequest extends BackendlessJsonObjectRequest {

    private static final String BOUNDARY = "__X_JOURNALING_APP_BOUNDARY__";
    private static final String NEW_LINE = "\r\n";

    private File mFile;
    private String mName;
    private String mMimeType;

    public BackendlessUploadRequest(int method, String url, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, null, listener, errorListener);
    }

    public void setFile(File file, String name, String mimeType) {
        mFile = file;
        mName = name;
        mMimeType = mimeType;
    }

    @Override
    public String getBodyContentType() {
        return String.format("multipart/form-data; charset=utf-8; boundary=%s", BOUNDARY);
    }

    @Override
    public byte[] getBody() {
        if (mFile == null) {
            return null;
        }

        try {
            FileInputStream fileInputStream = new FileInputStream(mFile);
            byte[] buffer = new byte[fileInputStream.available()];
            fileInputStream.read(buffer);
            fileInputStream.close();

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            StringBuilder partHeader = new StringBuilder();
            partHeader.append(String.format("--%s%s", BOUNDARY, NEW_LINE));
            partHeader.append(String.format("Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"%s", mName, mFile.getName(), NEW_LINE));
            partHeader.append(String.format("Content-Type: %s%s", mMimeType, NEW_LINE));
            partHeader.append(String.format("Content-Length: %d%s", buffer.length, NEW_LINE));

            outputStream.write(partHeader.toString().getBytes());
            outputStream.write(NEW_LINE.getBytes());

            // Write file bytes into body
            outputStream.write(buffer);

            outputStream.write(NEW_LINE.getBytes());
            outputStream.write(String.format("--%s--", BOUNDARY).getBytes());

            byte[] body = outputStream.toByteArray();
            outputStream.close();
            return body;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
