package com.apptivitylab.learn.journaling.ui;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apptivitylab.learn.journaling.R;
import com.apptivitylab.learn.journaling.model.Journal;

/**
 * A simple {@link Fragment} subclass.
 */
public class JournalDetailFragment extends Fragment {
    private static final String ARGS_JOURNAL = "journalsArgs";

    private Journal mJournal;
    private TextView mBodyTextView;
    private TextView mTitleTextView;

    public JournalDetailFragment() {
        // Required empty public constructor
    }

    public static JournalDetailFragment newInstance(Journal journal) {
        JournalDetailFragment fragment = new JournalDetailFragment();

        Bundle arguments = new Bundle();
        arguments.putParcelable(ARGS_JOURNAL, journal);

        fragment.setArguments(arguments);

        return fragment;
    }

    private Journal getJournal() {
        return mJournal;
    }

    private void setJournal(Journal journal) {
        mJournal = journal;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().getParcelable(ARGS_JOURNAL) != null) {
            mJournal = getArguments().getParcelable(ARGS_JOURNAL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_journal_detail, container, false);
        mTitleTextView = (TextView) view.findViewById(R.id.fragment_journal_detail_tv_journal_title);
        mBodyTextView = (TextView) view.findViewById(R.id.fragment_journal_detail_tv_journal_entry);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mTitleTextView.setText(mJournal.getTitle());
        mBodyTextView.setText(mJournal.getBody());
    }
}
