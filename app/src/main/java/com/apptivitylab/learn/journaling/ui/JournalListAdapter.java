package com.apptivitylab.learn.journaling.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apptivitylab.learn.journaling.R;
import com.apptivitylab.learn.journaling.model.Journal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jkhong on 9/22/16.
 */

public class JournalListAdapter extends RecyclerView.Adapter {
    private static final int VIEWTYPE_HEADER = 0;
    private static final int VIEWTYPE_CONTENT = 1;

    private final List<Integer> mSortedMonths = new ArrayList<>();
    private List<Journal> mJournalList = new ArrayList<>();
    private Map<Integer, List<Journal>> mSortedJournalMap = new HashMap<>();
    private JournalViewHolder.OnSelectJournalListener mJournalListener;

    public JournalListAdapter(JournalViewHolder.OnSelectJournalListener journalListener) {
        mJournalListener = journalListener;
    }

    public void setJournalList(List<Journal> journalList) {
        mJournalList.clear();
        mJournalList.addAll(journalList);
        sortJournalByMonths();
    }

    public void addJournal(Journal journal) {
        mJournalList.add(journal);
        sortJournalByMonths();
    }

    private void sortJournalByMonths() {
        mSortedJournalMap.clear();

        for (Journal journal : mJournalList) {
            int month = journal.getMonth();

            List<Journal> currentMonthJournals = mSortedJournalMap.get(month);
            if (currentMonthJournals == null) {
                currentMonthJournals = new ArrayList<>();
                mSortedJournalMap.put(month, currentMonthJournals);
            }
            currentMonthJournals.add(journal);
        }

        mSortedMonths.clear();
        mSortedMonths.addAll(new ArrayList<Integer>(mSortedJournalMap.keySet()));
        Collections.sort(mSortedMonths);
    }

    private Object getItem(int positon) {
        int itemPosition = 0;
        for (int month : mSortedMonths) {
            if (itemPosition == positon) {
                return month;
            }
            itemPosition++;

            List<Journal> journals = mSortedJournalMap.get(month);
            for (Journal journal : journals) {
                if (itemPosition == positon) {
                    return journal;
                }

                itemPosition++;
            }
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);

        Object item = getItem(position);
        if (item instanceof Integer) {
            return VIEWTYPE_HEADER;
        }
        return VIEWTYPE_CONTENT;
    }

    @Override
    public int getItemCount() {
        return mJournalList.size() + mSortedJournalMap.keySet().size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder holder = null;

        switch (viewType) {
            case VIEWTYPE_HEADER:
                View headerView = inflater.inflate(R.layout.cell_header, parent, false);
                holder = new HeaderViewHolder(headerView);
                break;

            case VIEWTYPE_CONTENT:
                View itemView = inflater.inflate(R.layout.cell_journal, parent, false);
                holder = new JournalViewHolder(itemView, mJournalListener);
                break;
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder) {
            Integer month = (Integer) getItem(position);
            Journal journal = mSortedJournalMap.get(month).get(0);

            ((HeaderViewHolder) holder).setHeaderTitle(journal);
        } else {
            Journal journal = (Journal) getItem(position);
            ((JournalViewHolder) holder).setJournalEntry(journal);
        }
    }
}
