package com.apptivitylab.learn.journaling.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.android.volley.toolbox.StringRequest;
import com.apptivitylab.learn.journaling.R;
import com.apptivitylab.learn.journaling.model.User;

/**
 * Created by aarief on 30/09/2016.
 */

public class UserController {

    private static UserController sUserController;

    private User mLoggedInUser;

    private UserController() {

    }

    public static synchronized UserController getInstance() {
        if (sUserController == null) {
            sUserController = new UserController();
        }

        return sUserController;
    }

    public User getLoggedInUser() {
        return mLoggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        mLoggedInUser = loggedInUser;
    }

}
