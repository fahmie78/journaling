package com.apptivitylab.learn.journaling.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by jkhong on 9/22/16.
 */

public class Journal implements Parcelable {
    private String mObjectId;
    private String mTitle;
    private String mBody;
    private Calendar mCreationDate;
    private Calendar mModifiedDate;

    //region Constructors
    public Journal() {
        mTitle = "Untitled";
        mCreationDate = Calendar.getInstance();
    }

    public Journal(String title, String body, Calendar creationDate) {
        mTitle = title;
        mBody = body;
        mCreationDate = creationDate;
    }

    public Journal(@NonNull JSONObject jsonObject) throws Exception {
        mTitle = jsonObject.getString("title");
        mBody = jsonObject.getString("body");

        Long creationDateLong = jsonObject.getLong("creation_date");
        Calendar creationDate = Calendar.getInstance();
        creationDate.setTimeInMillis(creationDateLong);
        mCreationDate = creationDate;
    }

    public JSONObject toJsonObject() {
        JSONObject jsonProduct = new JSONObject();

        try {
            jsonProduct.put("title", this.getTitle());
            jsonProduct.put("body", this.getBody());

            Calendar creationDate = this.getCreationDate();
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyyy KK:mm:ss");
            String creationDateString = dateFormat.format(creationDate.getTime());

            jsonProduct.put("creation_date", creationDateString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonProduct;
    }
    //endregion

    //region Parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mObjectId);
        parcel.writeString(mTitle);
        parcel.writeString(mBody);
        parcel.writeSerializable(mCreationDate);
        parcel.writeSerializable(mModifiedDate);
    }

    protected Journal(Parcel in) {
        mObjectId = in.readString();
        mTitle = in.readString();
        mBody = in.readString();
        mCreationDate = (Calendar) in.readSerializable();
        mModifiedDate = (Calendar) in.readSerializable();
    }

    public static final Creator<Journal> CREATOR = new Creator<Journal>() {
        @Override
        public Journal createFromParcel(Parcel in) {
            return new Journal(in);
        }

        @Override
        public Journal[] newArray(int size) {
            return new Journal[size];
        }
    };
    //endregion

    //region Getters & Setters
    public String getObjectId() {
        return mObjectId;
    }

    public void setObjectId(String objectId) {
        mObjectId = objectId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String body) {
        mBody = body;
    }

    public Calendar getCreationDate() {
        return mCreationDate;
    }

    public Calendar getModifiedDate() {
        return mModifiedDate;
    }
    public void setModifiedDate(Calendar modifiedDate) {
        mModifiedDate = modifiedDate;
    }
    //endregion

    //region Methods
    public Integer getMonth() {
        return mCreationDate.get(Calendar.MONTH);
    }
    //endregion


}
